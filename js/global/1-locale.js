
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LANGUAGE CONFIG
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var supportedLanguages = [ "en", "fr", "es", "it" ];
var defaultLanguage = "en";

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: return the asked string in the current language or (if not found) in english
  ARGUMENTS: (
    key <string>,
    subkey <string>,
  )
  RETURN: string
*/
var locale; // function defined in global.js

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

var $languageSwitch = $("#language-switch");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LANGUAGE DETERMINATION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function determineLanguage (supportedLang) {

  var query = $$.url.getQueryObject();
  var language;

  if (query.l) language = query.l;
  if (!language) language = localStorage.getItem("language");

  else {

    var browserLanguage = window.navigator.userLanguage || window.navigator.language;

    // iterate supportedLanguages to see if browser language is supported
    var i = 0;
    while (!language && i < supportedLanguages.length) {
      language = $$.match(browserLanguage, "^"+ supportedLang[i]);
      i++;
    }

  };

  // return language or default
  if (_.indexOf(supportedLanguages, language) !== -1) {
    localStorage.setItem("language", language);
    return language;
  }
  else {
    console.info("language: '"+ language +"' not supported; switched to default");
    return defaultLanguage;
  };

};

function makeLanguageToggleButton (currentLanguageCode) {

  // set current language
  $languageSwitch.children("#current").html(currentLanguageCode).click(function () { changeLanguage(currentLanguageCode) });

  // generate list of available languages
  var $languagesList = $languageSwitch.children("#list");
  _.each(supportedLanguages, function(supportedLanguageCode){
    if (supportedLanguageCode != currentLanguageCode) $languagesList.a({ html: supportedLanguageCode, }).click(function () { changeLanguage(supportedLanguageCode) });
  });

};

function changeLanguage (askedLanguageCode) {
  $$.url.modifyQuery({ l: askedLanguageCode, }, true);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE FUNCTION TO GET STRINGS IN CURRENT LANGUAGE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeLocaleFunction (currentLocale, englishLocale) {
  locale = function (key, subkey) {

    var result;

    // get from current locale
    if (currentLocale && currentLocale[key]) {
      if (subkey) result = currentLocale[key][subkey]
      else result = currentLocale[key];
    };

    // get from english locale
    if (!result && englishLocale[key]) {
      if (subkey) result = englishLocale[key][subkey]
      else result = englishLocale[key];
      // return string set as "untranslated"
      if (_.isArray(result)) result = _.map(result, function (val) { return untranslated(val); })
      else result = untranslated(result);
    };

    return result;

  };
};

function untranslated (string) {
  return '<span class="untranslated">'+ string +'</span>';
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LOCALE INIT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function localeFetchedCallback (languageCode, currentLocale, englishLocale, callback) {

  // make locale function
  makeLocaleFunction(currentLocale, englishLocale);

  // make language button
  makeLanguageToggleButton(languageCode);

  // callback
  if (_.isFunction(callback)) callback(languageCode);

};

/**
  DESCRIPTION: determine language code, fetch locale, prepare locale switching button
  ARGUMENTS: ( ?callback <function(ø)> )
  RETURN: <void>
*/
function localeInit (callback) {

  /**
    DESCRIPTION: current language's code
    TYPE: <string> « e.g. "en" »
  */
  var languageCode = determineLanguage(supportedLanguages);

  // get english locale
  $.getJSON("locales/en.json").done(function (englishLocale) {
    // get current locale and callback
    if (languageCode != "en") $.getJSON("locales/"+ languageCode +".json").done(function (currentLocale) {
      localeFetchedCallback(languageCode, currentLocale, englishLocale, callback);
    }).fail(function (err) {
      console.error("Failed to log '"+ languageCode +"' locale. Maybe there's an error in the json formatting.");
    })
    // current locale is english, so callback directly
    else localeFetchedCallback(languageCode, englishLocale, englishLocale, callback);

  }).fail(function (err) {
    console.error("Failed to fetch english locale. Maybe there's an error in the json formatting.");
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
