
var $content = $("#content");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LOAD CATS SVGS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function loadFooterCats (callback) {

  $("#footer").load("images/home-footer.svg", function() {
    var footer = $("#footer svg"),
        width = footer.attr("width"),
        height = footer.attr("height");
    footer.removeAttr("width");
    footer.removeAttr("height");
    footer.get(0).setAttribute("viewBox", "0 0 " + width + " " + height);
    footer.find(".twinkle").removeAttr("style");
    if (callback) callback()
  });

};

function loadSiderCat (callback) {

  $("#side").load("images/home-side.svg", function() {
    var side = $("#side svg"),
        width = side.attr("width"),
        height = side.attr("height");
    side.removeAttr("width");
    side.removeAttr("height");
    side.get(0).setAttribute("viewBox", "0 0 " + width + " " + (10000 + +height));
    side.find(".twinkle").removeAttr("style");
    if (callback) callback()
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
