
var currentPageHref;

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SET CURRENT PAGE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function setCurrentPage (askedPage) {

  // set query
  var query = $$.url.getQueryObject();
  if (askedPage) query.p = askedPage
  else if (query.p) askedPage = query.p
  else askedPage = query.p = "about";
  $$.url.setQueryFromObject(query);

  // set currentPage
  currentPageHref = document.location.href;

  // set display
  $(".doc-page").hide();
  $(".doc-footer").hide();
  $(".doc-menu").removeClass("active");
  $(".doc-page[data-id='"+ askedPage +"']").show();
  $(".doc-footer[data-id='"+ askedPage +"']").show();
  $(".doc-menu[data-id='"+ askedPage +"']").addClass("active");
  $("#documentation-content").scrollTop(0);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  HANDLE NEXT AND PREVIOUS NAVIGATIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function fetchDocumentation (languageCode, callback) {

  $.ajax({
    type: "GET",
    url: "/dist/wiki-"+ languageCode +".html",
    success: callback,
    error: function (err) {
      $$.log.error("Error fetching documentation pages contents.");
      $$.log.error(err);
    },
  });

};

function appendDocumentationPagesContents (documentationPages) {

  // replace placeholders
  documentation = replacePlaceholders(documentationPages);

  // add documentation pages to page content
  $(documentation)
    // keep only documentation divs (just filters out spaces, in fact)
    .filter(".doc-page")
    // sort pages by their index specified in markdown files
    .sort(function (a, b) {
      return $(b).data("index") < $(a).data("index") ? 1 : -1;
    })
    // display pages and their title in menu
    .each(function () {
      var self = $(this);

      // display title in side menu
      $("#documentation-navigation").a({
        class: "doc-menu"+ (self.data("sub") ? " doc-submenu" : ""),
        text: self.data("title"),
        "data-id": self.data("id"),
      }).click(function (e) {
        setCurrentPage(self.data("id"));
      });

      // translate text of "< back" entry in menu
      $("#documentation-menu-back").text(locale("back"));

      // display page content
      $("#documentation-content").append($(this));

      // display lastUpdate footer
      $("#documentation-content").div({
        class: "doc-footer",
        text: locale("pageUpdate") +": "+ self.data("last_update"),
        "data-id": self.data("id"),
      });

    })
  ;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  INITIALIZER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function initializeDoc (languageCode) {

  fetchDocumentation(languageCode, function (documentationPages) {

    // display documentation pages contents
    appendDocumentationPagesContents(documentationPages);

    // get default about page, or whichever one is asked in query
    setCurrentPage();

  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
