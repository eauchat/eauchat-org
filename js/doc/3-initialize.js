

$(document).ready(function() {

  localeInit(function (languageCode) {

    // load sider cat
    loadSiderCat();

    // load documentation
    initializeDoc(languageCode);

  });

});

// make sure appropriate documentatio page is shown  on back button press
window.addEventListener("popstate", function (event) {
  // if two states are identical, jump one more state back
  if (currentPageHref === document.location.href) history.back()
  // refresh which page should be displayed
  else setCurrentPage();
});
