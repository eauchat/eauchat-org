
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  PLACEHOLDERS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var placeholders = {

  // mail
  mailtoEauchatEN: '<b>eauchat [(at)] eauchat.org</b>',
  mailtoEauchatFR: '<b>eauchat [(arobase)] eauchat.org</b>',
  mailtoEauchatIT: '<b>eauchat [(chiocciola)] eauchat.org</b>',
  mailtoEauchatES: '<b>eauchat [(arroba)] eauchat.org</b>',
  mailtoEauchatTilaEN: '<b>eauchat [(at)] tila.im</b>',
  mailtoEauchatTilaFR: '<b>eauchat [(arobase)] tila.im</b>',
  mailtoEauchatTilaIT: '<b>eauchat [(chiocciola)] tila.im</b>',
  mailtoEauchatTilaES: '<b>eauchat [(arroba)] tila.im</b>',
  gpgKeyDownload: 'https://eauchat.org/images/eauchat-pgp-public-key.asc',

  // matrix
  page_elementAndroid: 'https://f-droid.org/packages/im.vector.app/',
  page_elementDesktop: 'https://element.io/get-started#download',
  page_elementBrowser: 'https://chat.magari.noho.st/#/login',
  image_matrixConfigAndroid: [
    '<span class="doc-images">',
    '<img src="images/matrix/element_android-login-1.png" height="350px">',
    '<img src="images/matrix/element_android-login-2.png" height="350px">',
    '<img src="images/matrix/element_android-login-3.png" height="350px">',
    '<img src="images/matrix/element_android-login-4.png" height="350px">',
    '<img src="images/matrix/element_android-login-5.png" height="350px">',
    '</span>',
  ],
  image_matrixConfig: [
    '<span class="doc-images">',
    '<img src="images/matrix/element-login-1.png" width="500px">',
    '<img src="images/matrix/element-login-2.png" width="500px">',
    '<img src="images/matrix/element-login-3.png" width="500px">',
    '</span>',
  ].join(""),

  // password change instructions
  image_changePassword: [
    '<span class="doc-images">',
    '<img src="images/changePassword/1.png" width="500px">',
    '<img src="images/changePassword/2.png" width="500px">',
    '<img src="images/changePassword/3.png" width="500px">',
    '</span>',
  ].join(""),

  // liberapayLink: '<a href="#">', // TODO
  // page_donate: '<a onclick="changePage(\'donate\'); return false;" href="#">',
  // page_contribute: '<a onclick="changePage(\'contribute\'); return false;" href="#">',

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  REPLACE PLACEHOLDERS FUNCTION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function replacePlaceholders (string) {

  // replace by placeholders value
  _.each(string.match(/\%\%[A-Za-z_\$0-9]*\%\%/g), function(match){
    string = string.replace(match, placeholders[match.replace(/\%\%/g, "")]);
  });

  return string;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
