

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  BANNER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function activateBanner (messageCode) {
  $("#banner").addClass("active");
  $("#banner-text").text(locale("banner", messageCode));
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE MENUS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var $helloContainer, $descriptionsContainer, $appsContainer, $footerMushroom, $game, $submenus, apps;

function createMenus () {

  //
  //                              APPS LIST

  $appsContainer = $content.ul({
    id: "apps",
  });

  _.each(config.appsList, function (app) {

    $appsContainer.li({
      class: "fade-in",
    }).a({
      "data-app": app.name,
      class: "icon-"+ app.icomoon,
      href: app.href,
    });

  });

  apps = $appsContainer.children();

  //
  //                              DESCIPTIONS AND HELLO

  $descriptionsContainer = $content.div({ id: "descriptions", });
  $helloContainer = $content.div({ id: "hello", });

  //
  //                              CHILDREN SITES LIST

  $submenus = $content.div({ id: "submenus" });

  var $perso = $submenus.div({
    id: "perso",
    html: "¿want to open a site hosted here?",
  });

  _.each(config.childrenSites, function(childSite){
    $perso.a({
      href: childSite.href,
      html: childSite.name,
    });
  });

  //                              ¬
  //

}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MENUS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var menusSet = {

  //
  //                              HOVER

  hover: function () {

    // Handler when mouse comes in
    var hoverIn = function() {
      var app = $(this).attr("data-app");
      get$description(app).addClass("active");
      $helloContainer.addClass("hidden");
    };

    // Handler when mouse goes out
    var hoverOut = function() {
      var app = $(this).attr("data-app");
      get$description(app).removeClass("active");
      $helloContainer.removeClass("hidden");
    };

    // SET HOVER FOR EACH APP AND INFO ICON
    apps.find("a").hover(hoverIn, hoverOut);
    $("#doc-switching-button").find("a").hover(hoverIn, hoverOut);
    $("#rtc").find("a").hover(hoverIn, hoverOut);
    $("#profile").find("a").hover(hoverIn, hoverOut);

    // DO IT FOR THE MUSHROOM (personal pages)
    $footerMushroom.hover(hoverIn, hoverOut);
    $footerMushroom.click(function(){
      toggleSubmenu("perso");
    });

  },

  //
  //                              SUBMENUS

  submenus: function () {

    apps.each(function(i,d){

      var $appLink = $(d).find("a");

      // if there is a submenu, set to open it on icon click
      var href = $appLink.attr("href");


      if (href.match(/^\#submenu\-/)) {

        $appLink.click(function () {
          toggleSubmenu(href.replace(/^\#submenu\-/, ""));
        });

      };

    });

  },

  //
  //                              POSITION

  randomPosition: function () {

    apps.each(function(i,d){

      var $app = $(d)

      // set random positions // NOTE: if you reenable this you should also reenable at the bottom of this script the executing of menusSet.randomPosition on window resize
      // randomPosition($app, apps, $appsContainer, config.middleRectangle);

      // set non random positions
      var appAnchor = $app.find("a").data("app"); // get the app name
      var appObject = _.findWhere(config.appsList, { name: appAnchor, });
      $app.css(appObject.css);

    });

  },

  //                              ¬
  //

}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  UTILITIES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

//
//                              SUBMENUS

function toggleSubmenu (submenuName) {

  var $submenu = get$submenu(submenuName);
  $submenu.toggle();
  // $submenu(att)

}

//
//                              GET ELEMENTS

function get$description (app) {
  return $descriptionsContainer.find("div[data-description=" + app + "]");
}

function get$submenu (name) {
  return $("#submenus #"+ name);
}

//
//                              MAKE DESCRIPTIONS

function makeDescriptions (locale) {
  _.each(locale("pages"), function (app, appName) {

    var $appDescription = $descriptionsContainer.div({ "data-description": appName, });

    $appDescription.h1({ html: app.title, });
    $appDescription.p({ html: app.description, });

  });
}

//                              ¬
//

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  READY FUNCTION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makePageContents (locale) {

  if (config.maintenance && config.maintenance.active) activateBanner(config.maintenance.messageCode);

  createMenus();

  // hello
  $helloContainer.html(locale("hello") +'<br><span>'+ locale("helloMessage") +'</span>');
  $helloContainer.removeClass("hidden");

  makeDescriptions(locale);

  // set footer mushroom
  $footerMushroom = $("#perso");

  // set footer mushroom
  $game = $("#game").click(function () {
    if (!playOn) startPlay()
    else stopPlay();
  });

  // set submenus hover, random position, and submenu on click
  menusSet.hover();
  menusSet.submenus();
  menusSet.randomPosition();

  // rtc as cloud
  var rtcIcon = $$.random.entry(["cloudy1", "cloud2", "snowy", "lightning"]);
  $("#rtc a").html('<span class="icon-'+ rtcIcon +'"></span>');

  // if game asked
  var query = $$.url.getQueryObject();
  if (query.game) startPlay(true);
  // else fade in
  else setTimeout(function () {
    $(".fade-in").css("opacity", 1);
  }, 300);

};

// NOTE: random position has been disabled on resize, since the position is not random anymore (if you reenable random position, you should also unquote the following)
// window.onresize = function(event) {
//   if (!playOn) menusSet.randomPosition();
// };

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
