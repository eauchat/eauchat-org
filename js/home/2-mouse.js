var $all = $("body");
var mousemouseIndex = 0;

function mousemouse (cursorPosition) {

  mousemouseIndex++;

  // only do it one time on 2
  if (mousemouseIndex % 2 != 0) return;
  // do not do it in top right corner
  if (cursorPosition.top < 150 && cursorPosition.left > window.innerWidth - 200) return;

  // create image at cursor position
  var $mou = $all.div({ class: "mousemouse fade-out icon-navigation", });
  cursorPosition.top = cursorPosition.top + $$.random.number(-30, 10);
  cursorPosition.left = cursorPosition.left + $$.random.number(-30, 10);
  $mou.css(cursorPosition)
  // random image size
  $mou.css("font-size", $$.random.number(10, 30) +"px");
  // remove image after 2 seconds (fading out with same speed)
  setTimeout(function () { $mou.remove(); }, 2000);

};
