
var config = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAINTENANCE MODE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  maintenance: {
    // active: true,
    // messageCode: "problem",  // will display a banner saying there is some issue being resolved
    // messageCode: "working"   // will display a banner saying there is some maintenance without specifying when it should end
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GENERAL
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  middleRectangle: {
    height: 200,
    width: 500,
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  APPS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  appsList: [

    //
    //                              COMMUNICATE

    {
      name: "emails",
      icomoon: "envelope",
      href: "https://mail.eauchat.org/",
      css: {
        left: "20%",
        top: "16%",
      },
    },

    {
      name: "chat",
      icomoon: "bubbles2",
      // href: "https://eauchat.org/wp/doc/chat/",
      // href: "?p=configureMatrix",
      href: "https://chat.magari.noho.st/#/login",
      css: {
        left: "43%",
        top: "0%",
      },
    },

    //
    //                              COLLABORATE

    {
      name: "pads",
      icomoon: "document-text3",
      href: "https://pad.eauchat.org",
      css: {
        right: "19%",
        top: "17%",
      },
    },

    // {
    //   name: "calcs",
    //   icomoon: "document-table1",
    //   href: "https://calc.eauchat.org",
    //   css: {
    //     right: "0%",
    //     top: "36%",
    //   },
    // },
    //
    // {
    //   name: "facilmap",
    //   icomoon: "compass1",
    //   href: "https://facilmap.eauchat.org/",
    //   css: {
    //     right: "12%",
    //     bottom: "32%",
    //   },
    // },

    {
      name: "poll",
      icomoon: "stats-bars",
      href: "https://poll.eauchat.org/",
      css: {
        right: "5%",
        bottom: "32%",
      },
    },

    //
    //                              STORE SHIT

    {
      name: "videos",
      icomoon: "film3",
      href: "https://video.eauchat.org/",
      css: {
        left: "0%",
        bottom: "22%",
      },
    },

    {
      name: "cloud",
      icomoon: "folder",
      href: "https://cloud.eauchat.org/",
      css: {
        left: "24%",
        bottom: "13%",
      },
    },

    // //
    // //                              YOUR CORNER
    //
    // {
    //   name: "websites",
    //   icomoon: "compass1",
    //   href: "https://eauchat.org/wp/wp-login.php",
    //   css: {
    //     left: "55%",
    //     bottom: "0%",
    //   },
    // },

    //
    //                              DATO

    {
      name: "dato",
      icomoon: "dato",
      href: "https://dato.eauchat.org",
      css: {
        left: "10%",
        bottom: "0%",
      },
    },

  ],

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SITES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  childrenSites: [

    {
      name: "chateau",
      href: "https://chateau.eauchat.org/",
    },

    {
      name: "squeak",
      href: "https://squeak.eauchat.org/",
    },

    {
      name: "desenredos",
      href: "https://desenredos.eauchat.org/",
    },

    {
      name: "mandelukogia",
      href: "https://mandelukogia.eauchat.org/",
    },

    {
      name: "slvh",
      href: "https://slvh.fr",
    },

    {
      name: "seriousblabla",
      href: "https://seriousblabla.eauchat.org/",
    },

    {
      name: "labpianoterra",
      href: "https://labpianoterra.eauchat.org/",
    },

    {
      name: "magari",
      href: "https://magari.eauchat.org/",
    },

    {
      name: "witnessing",
      href: "https://witnessing.eauchat.org/",
    },

    {
      name: "radiomagari",
      href: "https://radiomagari.eauchat.org/",
    },

    {
      name: "blatta",
      href: "https://blatta.eauchat.org/",
    },

  ],

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

};
