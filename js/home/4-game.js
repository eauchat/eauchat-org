
var playOn = false;
var $gameContainer;
var currentMousePos = { x: -1, y: -1 };
var invincible = true;
var $cursor;
var $newContainer;

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  NEW IMAGE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// before, new image used to get created automatically, but in the end it seems that localStorage state or something else wasn't properly coded and it was always showing
// if (localStorage.getItem("eauchat.org/game/newButton") != "neverShowAgain" && getAskedPage() == "index") showNewImage();

function showNewImage () {

  $newContainer = $("body").a({ id: "new", }).click(function(){ startPlay(); })
  var $newImage = $newContainer.div({ id: "new-image", });
  function blinkNew () {
    if ($newImage.css("opacity") == "1") $newImage.css("opacity", 0)
    else $newImage.css("opacity", 1);
    // $newImage.toggle();
  };
  $(document).ready(function() {
    setInterval(blinkNew, 50);
  });

  animateDiv($newContainer);

  localStorage.setItem("eauchat.org/game/newButton", false);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  START STOP
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function startPlay (bypassConfirm) {
  var fullMessage = locale("game", "start") +"\n\____________________________________\n\n"+ locale("game", "shortcuts");
  if (bypassConfirm || confirm(fullMessage)) {

    // $("#apps").css("visibility", "hidden");
    $("ul#apps li").css("opacity", 0); // this way it will have a transition
    $("#descriptions").hide();
    $("#hello").hide();
    $("body").addClass("gaming");

    // set query
    setGameInQuery();

    // cursor
    $cursor = $("body").div({ id: "cursor", });
    startCountDown();

    // cats
    displayCats();

    playOn = true;

    // remove new
    localStorage.setItem("eauchat.org/game/newButton", "neverShowAgain");
    if ($newContainer) $newContainer.remove();

  };
}

function stopPlay () {

  // $("#apps").css("visibility", "visible");
  $("ul#apps li").css("opacity", 1); // this way it will have a transition
  $("#descriptions").show();
  $("#hello").show();
  $("body").removeClass("gaming");

  // cursor
  $cursor.remove();

  // cats
  $gameContainer.remove();

  // set query
  removeGameFromQuery();

  playOn = false;

}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  COUNTDOWN
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function setGameInQuery () {
  var query = $$.url.getQueryObject();
  query.game = true;
  $$.url.setQueryFromObject(query);
};
function removeGameFromQuery () {
  var query = $$.url.getQueryObject();
  delete query.game;
  $$.url.setQueryFromObject(query);
};

function startCountDown () {

  invincible = true;
  $cursor.text(locale("game", "ready"));
  $cursor.css({ left: currentMousePos.x, top: currentMousePos.y, });
  setTimeout(function () {
    $cursor.text("3");
    setTimeout(function () {
      $cursor.text("2");
      setTimeout(function () {
        $cursor.text("1");
        setTimeout(function () {
          $cursor.html("<img src='images/game/mouse-turned.png'>");
          $cursor.addClass("gaming");
          invincible = false;
        }, 1000);
      }, 1000);
    }, 1000);
  }, 1000);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY CATS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var $catContainers = {};

function displayCat ($container, app) {

  $catContainers[app.name] = $container.div({ class: "cat-container", });
  $catContainers[app.name].img({
    class: "game-cat",
    src: "images/game/cats/"+ app.name +".png",
  }).hover(function () {
    if (!invincible) touchedCat(app);
  })

  var appPosition = $("#apps [data-app="+ app.name +"]").parent().offset();
  $catContainers[app.name].css(appPosition);

  // START MOVING AFTER ONE SECOND (OPACITY TRANSITION LENGTH)
  setTimeout(function () {
    animateDiv($catContainers[app.name]);
  }, 750);

}

function displayCats () {

  // create game container
  $gameContainer = $("#content").div({ id: "game-container", });

  // display each cat
  _.each(config.appsList, function (app) { displayCat($gameContainer, app) });

  // show cats with opacity transision
  $gameContainer.css("opacity", 1);

}

function touchedCat (app) {
  if (confirm(locale("game", "lost").replace("%%appName%%", '"'+ app.name +'"'))) window.location.href = app.href // window.open(app.href, "_blank");
  else startCountDown();
}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  RANDOM MOVEMENTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function animateDiv ($div) {
  var newq = makeNewPosition();
  $div.animate(
    { top: newq[0], left: newq[1] },
    {
      duration: $$.random.number(400, 1400),
      complete: function() {
        animateDiv($div);
      },
    }
  );
};

function makeNewPosition () {

  // Get viewport dimensions (remove the ~dimension of the div)
  var h = $(window).height() - 80;
  var w = $(window).width() - 140;

  var nh = Math.floor(Math.random() * h);
  var nw = Math.floor(Math.random() * w);

  return [nh, nw];

}

// //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
// //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
// //                                                  CHECK IF IN IFRAME NOTE: THIS IS NOT NECESSARY, SHORTCUTS ARE NOT FIRED WHEN HIDDEN
// //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//
// var in_overlay_iframe = (window.location != window.parent.location);
// var isInIframeAndHidden = false;
//
// if (in_overlay_iframe) {
//
//   var ynhOverlay = window.parent.document.getElementById("ynh-overlay");
//   if (ynhOverlay && ynhOverlay.style.visibility != "hidden") isInIframeAndHidden = true;
//
// };

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GLOBAL EVENTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// DETECT MOUSE POSITION
jQuery(function($) {
  $(document).mousemove(function(event) {
    currentMousePos.x = event.pageX;
    currentMousePos.y = event.pageY;
    if (playOn) $cursor.css({ left: event.pageX, top: event.pageY, })
    else mousemouse({ left: event.pageX, top: event.pageY, });
  });
});

// QUIT GAME CLKING ESC KEY
// var pressedKeys = {};
$(document).keyup(function(e) {
  // if (e.key == "Shift") pressedKeys.shift = false;
}).keydown(function (e) {
  if (e.key === "Escape" && playOn) stopPlay()
  // else if (e.key == "Shift") pressedKeys.shift = true
  // else if (pressedKeys.shift && e.key == "G") startPlay();
  else if (e.key == "G" && !playOn) startPlay()
  else if (e.key == "N" && !playOn) showNewImage();
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
