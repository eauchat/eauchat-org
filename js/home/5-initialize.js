
$(document).ready(function() {

  localeInit(function (languageCode) {

    loadSiderCat();
    loadFooterCats(function () {
      // start making contents (now that wave is created)
      makePageContents(locale);
    });

  });

});
