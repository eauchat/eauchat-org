
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  IMPROVE JQUERY
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
/**
  DESCRIPTION: adds some more useful methods to jQuery object
  ARGUMENTS: ( $ <jquery> )
  RETURN: $ <jquery>
*/
function improveJquery ($) {

  window.tagsCreator = { /////////////////////// WHY DOES IT HAS TO BE IN WINDOW TO WORK??
    div: "<div/>",
    a: "<a/>",
    img: "<img>",
    span: "<span/>",
    p: "<p/>",
    h1: "<h1/>",
    svg: "<svg/>",
    g: "<g/>",
    path: "<path/>",
    canva: "<canvas>",
    // canvas: "<canvas>", // not this or no canvas works anymore
    object: "<object/>",
    embed: "<embed>",
    link: "<link>",
    formDiv: "<form/>",
    video: "<video/>",
    audio: "<audio/>",
    source: "<source/>",
    iframe: "<iframe/>",
    fieldset: "<fieldset/>",
    legend: "<legend/>",
    // inputs
    input: "<input/>",
    label: "<label/>",
    buttonDiv: "<button>",
    textarea: "<textarea/>",
    // selects inputs
    selectDiv: "<select/>",
    option: "<option/>",
    optionDiv: "<option/>",
    // tables
    table: "<table/>",
    thead: "<thead/>",
    tbody: "<tbody/>",
    td: "<td/>",
    th: "<th/>",
    tr: "<tr/>",
    // lists
    ul: "<ul/>",
    li: "<li/>",
    //
    br: "<br/>",
  };

  for (key in tagsCreator) $.fn[key] = new Function ("attr", "append", "append = append || 'append'; return $(tagsCreator['"+ key +"'], attr)[append +'To'](this)");

  return $;

};

improveJquery($);

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SCRIPT LOADER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// // scripts loader
// function loadScript (url, callback) {
//   var script = document.createElement('script');
//   script.type = 'text/javascript';
//   script.src = url;
//   // There are several events for cross browser compatibility.
//   script.onreadystatechange = callback;
//   script.onload = callback;
//   // Fire the loading
//   document.head.appendChild(script);
// };

// function loadStyle (url, callback) {
//   var style = $('<link/>', {
//     rel: 'stylesheet',
//     type: 'text/css',
//     href: url,
//     // class: classToAdd,
//   }).appendTo('head');//.appendTo('#loaded_styles');
//   style.ready(callback);
// };

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  RANDOM POSITION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/


var tooManyIterations;
var middleRectangle;
function randomPosition (element, elements, container, defaultMiddleRectangle) {

  tooManyIterations = 0;
  middleRectangle = { width: defaultMiddleRectangle.width, height: defaultMiddleRectangle.height }

  var callindId = generateId();

  // MAKE SURE ELEMENTS HAVE IDS
  elements.each(function(i,d){
    var element = $(d);
    if (!element.attr("id")) element.attr("id", "random-position-id-"+ callindId +"-"+ i);
  });

  //----------------------------------------------- NO OVERLAPING
  if (elements.length > 1) {

    // store boxes positions
    var boxDims = [rectangleInMiddle(container)]; // NOTE: ADD CUSTOM RECTANGLE IN CENTER NOT TO OVERLAP WITH TITLE
    var thisBox;

    function getBoxDim (elem) {
      return {
        // top: elem.css("top"),
        x: parseInt(window.getComputedStyle(elem).left),
        y: parseInt(window.getComputedStyle(elem).top),
        width: parseInt(window.getComputedStyle(elem).width),
        height: parseInt(window.getComputedStyle(elem).height),
      }
    };

    elements.each(function(i,d){
      // get dimensions
      var boxDimensions = getBoxDim($(d)[0]);
      // store dimensions for current box and others in different places
      if ($(d).attr("id") != element.attr("id")) boxDims.push(boxDimensions)
      else thisBox = boxDimensions;
    });

    //
    var conflict = true;
    while (conflict) {
      // generate new position
      var box = {
        x: $$.random.number(0, container.width() - element.width()),
        y: $$.random.number(0, container.height() - element.height()),
        width: thisBox.width,
        height: thisBox.height,
      };
      // check if new position overlap other boxes
      for (var i=0; i<boxDims.length; i++) {
        if (isCollide(box, boxDims[i])) {
          conflict = true;
          break;
        } else {
          conflict = false;
        }
      };

      tooManyIterations++;

      // security if looping too much
      if (tooManyIterations == 500) middleRectangle = { width: 0, height: 0 };
      if (tooManyIterations == 1000) break;

    };

    // set new position now that it's sure there is no overlap
    $(element).css({
      left: box.x,
      top: box.y,
    });

  }
  //----------------------------------------------- OVERLAPING
  else {
    element.css({
      left: $$.random.number(0, container.width() - element.width()),
      top: $$.random.number(0, container.height() - element.height()),
    });
  };

};

// check if two rectangle overlap each other
function isCollide (a, b) {
  // var ay = a.y || a.top;
  // var by = a
  return !(
    ((a.y + a.height) < (b.y)) ||
    (a.y > (b.y + b.height)) ||
    ((a.x + a.width) < b.x) ||
    (a.x > (b.x + b.width))
  );
};

function generateId () {
  return randomString() + randomString() + randomString();
}

function randomString () { return Math.random().toString(36).substring(7); }

function rectangleInMiddle ($container) {
  // var width = appsContainer.width() / 2;
  // var height = appsContainer.height() / 2;
  return {
    width: middleRectangle.width,
    height: middleRectangle.height,
    x: ( $container.width() - middleRectangle.width ) / 2,
    y: ( $container.height() - middleRectangle.height ) / 2,
  };
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
