//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SQUEAK QUERY (adapted extract of squek library)
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var $$ = {

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  STRING
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: match something in a string, and return it, or null
    API: (
      string <string|any> « if any, will return null »,
      matcher <regexp|string|[string<match>, string«flags»]>,
    )
    RETURN: null | string
  */
  match: function (string, matcher) {

    if (!_.isString(string)) return null
    else {

      // make matcher a regexp if it's not
      if (_.isString(matcher)) matcher = new RegExp (matcher)
      else if (_.isArray(matcher)) matcher = new RegExp (matcher[0], matcher[1]);
      // match
      var isMatch = string.match(matcher);

      // return null if no match
      if (!isMatch) return null
      // otherwise return firt match
      else return isMatch[0];

    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  JSON
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  json: {

    /*
      DESCRIPTION: just a secure version of JSON.stringify(object)
      ARGUMENTS: ( object <any> )
      RETURN: json string
      TODO: add ability to handle anything
    */
    short (object) {
      return JSON.stringify(object);
    },

    /*
      DESCRIPTION: improved version of JSON.stringify(object, null, "\t")
      ARGUMENTS: (
        object <any compatible with JSON.stringify>,
        morePretty <boolean> «if true array will not be indented, so it will take less useless space»
      )
      RETURN: json string
    */
    pretty (object, morePretty) {
      // var string = JSON.stringify(object, null, 2); // 2 spaces instead of tab
      let string = JSON.stringify(object, null, "\t");
      // do not exten arrays, otherwise it takes too much space
      if (morePretty) _.each(string.match(/\[[^\]\[\{\}]*\]/g), (subString,i) => {
        // remove spaces and line jumps
        let prettyFiedSubString = JSON.stringify(JSON.parse(subString));
        string = string.replace(subString, prettyFiedSubString);
        // string = string.replace(subString, subString.replace(/\s/g, "").replace(/\n/g, "").replace(/,/g, ", ")); // will have error if strings contain , or spaces in
      });
      return string;
    },

    /*
      DESCRIPTION: get a js object in a JSON format to copy
      ARGUMENTS: ( object <any compatible with JSON.stringify> )
      RETURN: void > copy text (through prompt)
    */
    copy (object) {
      prompt("Copy object json?", json.pretty(object))
    },

    /**
      DESCRIPTION: return true is a valid JSON, false otherwise
      ARGUMENTS: ( string )
      RETURN: boolean
    */
    isJson (string) {

      try { JSON.parse(string); return true }
      catch (e) { return false; }

    },

    /**
      DESCRIPTION: if provided value is JSON string, parse it, otherwise just return it
      ARGUMENTS: ( string )
      RETURN: any
    */
    parseIfJson (val) {

      try { return JSON.parse(val); }
      catch (e) { return val }

    },

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  URL
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  url: {

    /**
      DESCRIPTION: get the current hash
      ARGUMENTS: ( ø )
      RETURN: string
    */
    getHash: function () {
      return window.location.hash.replace(/^\#/, "");
    },

    /**
      DESCRIPTION: set hash to given value
      ARGUMENTS: ( hash <string> )
      RETURN: string
    */
    setHash: function (hash) {
      if (!hash) hash = "";
      if (!_.isString(hash)) return console.log("$$.url.setHash", "hash must be a string", hash, "is not a string");
      else return window.location.hash = hash;
    },

    /**
      DESCRIPTION: get this url query string
      ARGUMENTS: ( ø )
      RETURN: QueryString
    */
    getQueryString: function () {
      return squeak.string.replace(location.search, /^\?/, "") || "";
    },

    /**
      DESCRIPTION: get this url query as an object
      ARGUMENTS: ( ?query « if nothing passed here or it's not a string, will get query from page's url » )
      RETURN: QueryObject
    */
    getQueryObject: function (query) {
      var match,
      pl     = /\+/g,  // Regex for replacing addition symbol with a space
      search = /([^&=]+)=?([^&]*)/g,
      decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); };
      if (!_.isString(query)) query  = window.location.search.substring(1);

      urlParams = {};
      while (match = search.exec(query))
      urlParams[decode(match[1])] = decode(match[2]);

      return _.mapObject(urlParams, function(param){
        return $$.json.parseIfJson(param);
      });
    },

    /**
      DESCRIPTION: set page query string in url
      ARGUMENTS: (
        queryString: <string>,
        reload <boolean> « pass true to reload page on query change »,
      )
      RETURN: void
    */
    setQueryFromString: function (queryString, reload) {

      if (history.pushState) {

        var newurl = window.location.protocol +"//"+ window.location.host + window.location.pathname +"?"+ queryString;
        window.history.pushState({ path: newurl }, "", newurl);

        // reload page if asked
        if (reload) document.location.reload();

      };

    },

    /**
      DESCRIPTION: set page query string in url from a query object
      ARGUMENTS: (
        queryObject: <QueryObject>,
        reload <boolean> « pass true to reload page on query change »,
      )
      RETURN: QueryString
    */
    setQueryFromObject: function (queryObject, reload) {

      var queryString = "";

      _.each(queryObject, function(val,key){

        // add &
        if (queryString) queryString += "&";

        // add key and value
        queryString += key +"="+ (_.isObject(val) ? JSON.stringify(val) : val);

      });

      // set query string in url
      $$.url.setQueryFromString(queryString, reload);

      // return query string
      return queryString;

    },

    /**
      DESCRIPTION: modify page query string in url from a query object (only specified keys are modified)
      ARGUMENTS: (
        object: <object>,
        reload <boolean> « pass true to reload page on query change »,
      )
      RETURN: QueryString
    */
    modifyQuery: function (newQueryObjectPortion, reload) {
      let newQueryObject = Object.assign($$.url.getQueryObject(), newQueryObjectPortion);
      return $$.url.setQueryFromObject(newQueryObject, reload);
    },

    /**
      DESCRIPTION: turn a string into a safe url escaping ["?", "#", "\s"]
      ARGUMENTS: (
        !string <string>,
        ?slashesAlso <boolean:default=false> « pass true here to also replace slashes by their representation »,
      )
      RETURN: string
    */
    urlify: function (string, slashesAlso) {
      var result = string.replace(/\?/g, "%3F").replace(/\#/g, "%23").replace(/\s/g, "%20");
      if (slashesAlso) result = result.replace(/\//g, "%2F");
      return result;
    },

    /**
      DESCRIPTION: turn a urlified string back into it's normal state
      ARGUMENTS: ( string )
      RETURN: string
    */
    unurlify: function (string) { return string.replace(/%3F/g, "?").replace(/%23/g, "#").replace(/%20/g, " ").replace(/%2F/g, "/"); },

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  RANDOM
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  random: {

    number: function (min, max) {
      if (_.isArray(min)) return $$.random.number(min[0], min[1])
      else if (_.isUndefined(max)) return min
      else return Math.random() * (max - min) + min;
    },

    integer: function (min, max) {
      if (_.isArray(min)) return $$.random.integer(min[0], min[1])
      else if (_.isUndefined(max)) return min
      else return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    entry: function (array) {
      return array[$$.random.integer(0, array.length-1)];
    },

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

};
