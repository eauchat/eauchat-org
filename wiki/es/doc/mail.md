---
title: "configurar un cliente de correo"
index: 2.2
sub: "doc"
lastUpdate: "2022-01-29"
---


## Cómo configurar un cliente de correo electrónico

Si quieres configurar un cliente de correo electrónico (K9-mail, Thunderbird…) puede que necesites elegir configuracion manual y completar los datos como se muestra a continuación:

| Protocolo | Servidor    | Puerto | Seguridad | Autenticación     | Nombre de usuario                    |  
| --------- | ----------- | ------ | --------- | ----------------- | ------------------------------------ |  
| IMAP      | eauchat.org | 993    | SSL/TLS   | Contraseña normal | nombre de usuario (sin @eauchat.org) |  
| SMTP      | eauchat.org | 587    | STARTTLS  | Contraseña normal | nombre de usuario (sin @eauchat.org) |  

Si no entiendes como hacer esta configuracion, puedes encontrar las instruccciones paso a paso en [el siguiente tutorial](https://yunohost.org/es/user_guide/emailclients).
