---
title: "usar el chat"
index: 2.3
sub: "doc"
lastUpdate: "2022-01-29"
---


## Cómo usar el chat de eauchat (cómo configurar un cliente matrix)

Para chatear a traves de eauchat, visitas [esta pagina](%%page_elementBrowser%%), o usas un cliente matrix siguiendo las instruccciones especificadas aqui.  

Puedes utilizar por ejemplo la applicación Element, se puede instalar en [smartphones](%%page_elementAndroid%%) y [ordenadores](%%page_elementDesktop%%).  

Para iniciar una sesión, simplemente utiliza tu nombre de usuario y contraseña, pero no te olvides de selecionar "eauchat.org" como "Homeserver".  

Para más detalles, ver a continuación.  

### En el ordenador

%%image_matrixConfig%%

(1) Asegurate de modificar la opción "Homeserver"  
(2) La dirección del servidor es: "eauchat.org"  
(3) Introduce el nombre de usuario y la contraseña que tienes para utilizar los servicios de eauchat  
(4) Haz click en el botón "Sign in"  

### En el smartphone

%%image_matrixConfigAndroid%%

(1) Haz click en el botón "Other"  
(2) La dirección del servidor es: "eauchat.org"  
(3) Haz click en el botón "Sign in"  
(4) Introduce el nombre de usuario y la contraseña que tienes para utilizar los servicios de eauchat  
(5) Haz click en el botón "Sign in"  
