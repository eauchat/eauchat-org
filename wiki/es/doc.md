---
title: "Configuración/documentación"
index: 2
lastUpdate: "2022-01-29"
---


## Configuración / documentación

- [Cómo cambiar tu contraseña](?p=password)
- [Cómo configurar un cliente de correo electrónico](?p=mail)
- [Cómo usar el chat de eauchat (configurar un cliente matrix)](?p=matrix)
