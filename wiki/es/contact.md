---
title: "Contact"
index: 5
lastUpdate: "2022-01-29"
---


## ¿Como contactarnos?

Para solicitar una cuenta, o si encuentras cualquier problema o necesita ayuda, puedes escribir a %%mailtoEauchatES%%.  

Si el servidor parece no funcionar, se puede que los correos de eauchat no funcionan, en este caso puedes escribir a %%mailtoEauchatTilaES%%.  

Si quieres encriptar tu mensaje, vas a necesitar la llave gpg publica de eauchat, [la puedes descargar aquí](%%gpgKeyDownload%%).  
