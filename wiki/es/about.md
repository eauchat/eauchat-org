---
title: "Sobre eauchat"
index: 1
lastUpdate: "2022-01-29"
---


## ¿Por qué eauchat?

Eauchat es un servidor asociativo construido por voluntarios para promover un Internet libre, descentralizado y abierto.  
Si quieres utilizar nuestros servicios (correo, nube, paginas, chat, llamadas...) puedes simplemente escribir a %%mailtoEauchatES%%. Pero no olvides que no somos google, no tenemos un ejercito de ingenieros trabajando día y noche. Llevamos este servicio lo mejor que podemos y con nuestras (limitadas) habilidades.  


## ¿Que puedo hacer con eauchat?

 - enviar y recibir correos
 - chatear con cualquier persona (que esta persona tenga eauchat o no)
 - guardar documentos, compartirlos, trabajarlos colectivamente
 - sincronizar y guardar mis contactos y calendario
 - trabajar de manera colaborativa sobre documentos
 - crear y gestionar listas de correo
 - hacer encuestas para organizarse colectivamente
 - crear mi propia pagina (con wordpress o desde cero)

Para crear una lista de correo o una nueva pagina, tienes que escribir a %%mailtoEauchatES%%.  
Y si te gustaría que haya otros servicios, puedes escribirnos y veremos lo que podemos hacer :)  
