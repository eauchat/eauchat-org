---
title: "Contribuye"
index: 4
lastUpdate: "2022-01-29"
---


## ¿Qué es esto de eauchat?

Eauchat es un servidor, y un servidor, pues, simplemente es un ordenador que siempre esta conectado a internet, en el cual hay servicios instalados (como aplicaciones y otras cosas), y puedes usarlos como quieras y desde donde quieras.

Este servidor -ordenador- esta en una oficina de Tetaneutral (que es un proveedor asociativo e independiente de internet) en Toulouse.

Así que sí, llevar un servidor requiere algunos costes: las partes para construir el servidor costaron 720€, y pagamos 260€/año a Tetaneutral para sus gastos y su trabajo (electricidad, banda ancha, esa clase de cosas). A estos costes, se le añaden 20€ anuales por poseer el dominio “eauchat.org” (si, si tu quieres un dominio de internet, tienes que alquilarlo ;)).

Pero eauchat también requiere mucho tiempo, invertido en construir el servidor, los servicios, mantenerlos actualizados, resolver problemas que puedan surgir, contestar preguntas de los usuarios... Es difícil estimar cuanto tiempo empleamos en ello ya que hay muchas tareas diferentes. Pero siempre nos ocupa al menos unas diez horas a la semana, o bastantes más cuando desarrollamos nuevos servicios o tenemos que resolver problemas importantes.


## Cómo ayudarnos

Todas las contribuciones son bienvenidas 🙂

Si quieres contribuir economicamente para cubrir gastos, puedes hacerlo cuando nos encontres, o a través de transferencia bancaria, pago online o enviando un cheque (toda la información necesaria esta al final de esta página). Puedes hacer donaciones puntuales o periódicas.

Si quieres participar de otras maneras, mandanos un correo a %%mailtoEauchatES%% y seguro que encontraremos algo interesante que puedas hacer con tus habilidades y motivación ;)

En cualquier caso, no olvides que solamente usando los servicios de eauchat (o de cualquier otro pequeño servidor local) en lugar de lo que ofrecen las grandes compañias, las cuales estan moldeando el mundo digital del mañana, ya es una maravillosa contribución para hacer del mundo un lugar mejor 🙂


## Para donar puedes

#### Hacer una transferencia bancaria:

Por ahora, eauchat no tiene cuenta bancaria, asi que si quieres hacer una transferencia, envianos un correo a %%mailtoEauchatES%% y te enviamos el contacto bancario privado de uno de nosotros.

#### Enviarnos un cheque:

De momento, como para las transferencias, puedes enviarnos un mensaje a %%mailtoEauchatES%% y te podemos enviar nuestra dirección.
