---
title: "À propos"
index: 1
lastUpdate: "2022-01-29"
---


## Pourquoi le serveur eauchat ?

Eauchat est un serveur associatif construit par quelques bénévoles dans le but de promouvoir un internet décentralisé et libre.  
Si vous souhaitez utiliser ses services (mail, cloud, sites, partage de documents, appels…) vous pouvez simplement en faire la demande en écrivant à l’adresse %%mailtoEauchatFR%%. Cependant gardez en tête que nous ne sommes pas google, et nous n’avons pas une armée d’ingénieur qui travaille en permanence. Nous gérons le service du mieux que nous pouvons, et avec nos capacités.  


## Qu'est-ce que je peux faire avec eauchat ?

 - envoyer et recevoir des mails
 - chatter avec n'importe qui (pas seulement des utilisateurs d'eauchat)
 - garder mes documents, les partager, travailler collectivement dessus
 - synchroniser et sauvegarder mes contacts et mon calendrier
 - travailler collaborativement sur des documents
 - créer et gérer des listes mail
 - faire des sondages pour s'organniser collectivement
 - créer mon propre site internet (avec wordpress ou de zéro)

Pour créer une mailing liste ou un site internet, faites en la demande à %%mailtoEauchatFR%%.  
Si vous souhaiteriez utiliser d'autres services, vous pouvez nous écrire pour nous le proposer et on verra ce qu'on peut faire pour ça :)  
