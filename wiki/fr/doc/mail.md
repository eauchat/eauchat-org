---
title: "configurer un client mail"
index: 2.2
sub: "doc"
lastUpdate: "2022-01-29"
---


## Comment configurer un client mail

Si vous souhaitez configurer votre boîte mail depuis un client mail (K9-mail, Thunderbird…) vous aurez sans doute, pour que cela fonctionne, à entrer la configuration suivante manuellement:

| Protocole | Serveur     | Port | Chiffrement | Authentification    | Login                                |  
| --------- | ----------- | ---- | ----------- | ------------------- | ------------------------------------ |  
| IMAP      | eauchat.org | 993  | SSL/TLS     | Mot de passe normal | nom_dutilisateur (sans @eauchat.org) |  
| SMTP      | eauchat.org | 587  | STARTTLS    | Mot de passe normal | nom_dutilisateur (sans @eauchat.org) |  

Si vous ne comprenez pas très bien comment faire cette configuration, [la page suivante](https://yunohost.org/fr/user_guide/emailclients) explique en détail la procédure.
