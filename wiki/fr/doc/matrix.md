---
title: "utiliser le chat"
index: 2.3
sub: "doc"
lastUpdate: "2022-01-29"
---


## Comment utiliser le chat eauchat (configurer un client matrix)

Pour utiliser le chat d’eauchat, vous pouvez visiter [cette page](%%page_elementBrowser%%), ou bien utiliser un client matrix en suivant les instructions ci-dessous.

Vous pouvez utiliser par exemple Element qui existe sous forme d’application [pour smartphone](%%page_elementAndroid%%) ou [pour ordinateur](%%page_elementDesktop%%).

Lorsque que vous vous connectez avec le client de votre choix, veillez bien à entrer "eauchat.org" dans le champ "Homeserver".

Pour plus de détails, voir les illustrations ci-dessous.

### Sur ordinateur

%%image_matrixConfig%%

(1) Assurez vous de choisir l’option "Homeserver"  
(2) L’addresse à entrer est: "eauchat.org  
(3) Entrez votre nom d’utilisateur et mot de passe  
(4) Cliquez sur "Sign in"  

### Sur téléphone

%%image_matrixConfigAndroid%%

(1) Choisissez l’option "Other"  
(2) L’addresse à entrer est: "eauchat.org  
(3) Choisissez "Sign in"  
(4) Entrez votre nom d’utilisateur et mot de passe  
(5) Cliquez sur "Sign in"  
