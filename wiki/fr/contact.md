---
title: "Contact"
index: 5
lastUpdate: "2022-01-29"
---


## Comment nous contacter ?

Pour toute demande, de créatio de compte, en cas de problème ou si vous avez besoin d'aide, le plus simple est d'écrire à %%mailtoEauchatFR%%.  

Si jamais le serveur semble hors service, les mails vers les addresses eauchat.org risquent de ne pas fonctionner, dans ce cas vous pouvez nous contacter à %%mailtoEauchatTilaFR%%.  

Si vous souhaitez chiffrer votre message, vous pouvez utiliser la clé gpg public d'eauchat [téléchargeable ici](%%gpgKeyDownload%%).  
