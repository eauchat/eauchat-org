---
title: "Contribuer"
index: 4
lastUpdate: "2022-01-29"
---


## Eauchat concrètement c’est quoi ?

Un serveur, c’est à dire un ordinateur qui tourne en permanence, qui est branché au réseau internet, dans lequel sont installés les service auxquels vous pouvez accéder. Ce serveur est à Toulouse, dans les locaux de Tetaneutral, un fournisseur d’accès internet indépendant.

Faire tourner ce serveur a un coût (en plus des 720€ initiaux pour l'achat des pièces pour construire la machine), 260€/an payés à tetaneutral pour couvrir leurs frais et leur travail (électricité, bande passantes, ce genre de choses). À cela, s’ajoute le nom de domaine "eauchat.org" qui coûte une vingtaine d’euros par an (et oui, un nom de domaine ça se loue ;)).

Mais eauchat c’est aussi du temps, beaucoup de temps investi, pour construite le serveurs, pour installer les services sur le serveur, mais aussi les tenir à jour, régler les problèmes qui peuvent surgir, répondre aux questions et demandes des utilisateurs… C’est difficile d’estimer le temps qu’on y passe parce que les tâches sont diverse et disséminées. Mais c’est toujours au minimum quelques heures par semaines. Assez facilement une dizaine, voir vraiment plus pour déployer de nouveaux services.


## Et c’est possible de vous aider ?

Alors ça oui 🙂 on accueille tout type de contribution avec joie.

Si vous voulez participer financièrement, c’est bienvenu. Vous pouvez faire ça directement si vous croisez un d'entre nous, ou bien en envoyant un chèque, virement, ou paiement en ligne (toutes les infos sont dans le paragraphe si desous). Vous pouvez choisir de faire un don ponctuel, ou récurrent.

Et si vous souhaitez vous impliquer d’autres manières, c’est tout aussi bienvenu, écrivez nous un petit mail à %%mailtoEauchatFR%%, et je suis sûr qu’on trouvera quelque chose d’utile à faire qui matche bien vos compétences et envies !

Bon mais dans tout les cas, souvenez vous que rien que d’utiliser les services d’eauchat ou d’autres hébergeurs associatifs, plutôt que ceux des quelques grandes compagnies qui sont en train de modeler le monde connecté de demain, c’est déjà une super contribution à un monde un petit peu meilleur 🙂


## Faire un don

#### Par transfert:

Pour l'instant, eauchat n'a pas de compte en banque propre, donc si vous voulez nous faire un virement, envoyez nous un petit message a %%mailtoEauchatFR%% et on peut vous envoyer le RIB de l'un d'entre nous.

#### Envoyer un chèque:

Pour l'instant, comme pour les transferts, envoyez nous un mail à %%mailtoEauchatFR%% et on peut vous envoyer l'adresse et l'ordre.
