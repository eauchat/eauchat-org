---
title: "Configuration/documentation"
index: 2
lastUpdate: "2022-01-29"
---


## Configuration / documentation

- [Comment changer votre mot de passe](?p=password)
- [Comment configurer un client mail](?p=mail)
- [Comment utiliser le chat (configurer un client matrix)](?p=matrix)
