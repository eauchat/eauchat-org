

#### Par paiement en ligne:
Vous pouvez nous envoyer vos dont par liberapay, en suivant [ce lien](%%liberapayLink%%).


#### Hacer una donación online:
Puedes enviarnos dinero, mediante liberapay, en [este link](%%liberapayLink%%).

#### Fare un pagamento online:
Puoi inviarci un pagamento attraverso liberpay, [a questo link](%%liberapayLink%%).

----------------

"⚠ Si lors de la configuration, le client mail affiche une erreur de « certificat », il faut accepter et « ajouter une exception ». (Je travaille à la configuration du serveur pour qu’il n’affiche plus ce message, mais il n’y a aucun problème, c’est normal.)"

"⚠ Quando validi la configurazione, un errore di 'certificato' potrebbe apparire sul tuo client mail. In questo caso, accetta e 'aggiungi eccezione'. (Sto ancora lavorando sulla configurazione del sever perché smetta di mostrare questo messaggio, ma non c'é nessun problema nell'aggiungere l'eccezione, é un avviso normale.)"

----------------

  "canari": {

    "TODO": "C'EST PAS DU TOUT BIEN ÉCRIT CE QUI SUIT ;)",

    "pageTitle": "canari",

    "canariTitle": "Euh c'est quoi cette histoire de canari ?",
    "canariText": [
      "Le serveur eauchat étant situé en France, certaines lois s'appliquent. L'état peut ainsi forcer un hébergeur à mettre sur écoute une personne, sans pouvoir prévenir cette personne ou même avoir le droit légal de faire de déclaration à ce sujet.",
      "Pour lutter contre ce genre d'abus, de censure, le principe est de poster régulièrement une déclaration qui explique par example qu'aucune demande n'a été faite visant des utilisateurs du serveur. Si un jour de telles demandes sont faites, nous de publieront plus cette déclaration.",
      "Il est plus facile légalement de nous interdire de déclarer que le service a reçu des demandes de surveillance, que de nous forcer à mentir en postant qu'aucune demande n'a été reçue alors qu'en fait si."
    ]

  },
