---
title: "cambiare la tua password"
index: 2.1
sub: "doc"
lastUpdate: "2022-01-29"
---


## Come cambiare la password

Puoi cambiare la tua password dalla pagina principale seguendo le instruzioni qui sotto:

%%image_changePassword%%
