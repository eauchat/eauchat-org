---
title: "configurare un client mail"
index: 2.2
sub: "doc"
lastUpdate: "2022-01-29"
---


## Come configurare un client mail

Se vuoi configurare un client mail (K9-mail, Thunderbird...) avrai bisogno probabilmente di scegliere una configurazione manuale spiegata qui di seguito:

| Protocollo | Server      | Port | Criptaggio | Autenticazione   | Login                                   |  
| ---------- | ----------- | ---- | ---------- | ---------------- | --------------------------------------- |  
| IMAP       | eauchat.org | 993  | SSL/TLS    | Password normale | unome_utilizzatore (senza @eauchat.org) |  
| SMTP       | eauchat.org | 587  | STARTTLS   | Password normale | unome_utilizzatore (senza @eauchat.org) |  

Se non riesci a capire bene come fare questa configurazione, puoi trovare un tutorial che ti guida passo a passo nella procedura [alla pagina seguente](https://yunohost.org/it/user_guide/emailclients).
