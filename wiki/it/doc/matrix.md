---
title: "utilizzare la chat"
index: 2.3
sub: "doc"
lastUpdate: "2022-01-29"
---


## Come configurare un client matrix

Pe chattare attraverso eauchat, poi visitare [questa pagina](%%page_elementBrowser%%), o utilizzare un client matrix come spiegato qui.

Puoi usare per esempio Element, che esiste sotto forma di applicatione [per smartphone](%%page_elementAndroid%%) o [per computer](%%page_elementDesktop%%).

Per accedere, usa il tuo usuale nome d'utilizzatore e password ma non dimenticare di entrare "eauchat.org" come "Homeserver".

Per più dettagli, guarda le illustrazioni che seguono.

### Sul computer

%%image_matrixConfig%%

(1) Assicurati di modificare l'opzione "Homeserver"  
(2) L'indirizzo del server da inserire é: "eauchat.org"  
(3) Inserisci il tuo nome d'utilizzatrice e password  
(4) Clicca il tasto "Sign in"  

### Sul smartphone

"%%image_matrixConfigAndroid%%"

(1) Clicca il tasto "Other"  
(2) L'indirizzo del server da inserire é: "eauchat.org"  
(3) Clicca il tasto "Sign in"  
(4) Inserisci il tuo nome d'utilizzatrice e password  
(5) Clicca il tasto "Sign in"  
