---
title: "Configurazione/documentazione"
index: 2
lastUpdate: "2022-01-29"
---


## Configurazione / documentazione

- [Come cambiare la password](?p=password)
- [Come configurare un client mail](?p=mail)
- [Come utilizzare la chat (configurare un client matrix)](?p=matrix)
