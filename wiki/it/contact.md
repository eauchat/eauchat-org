---
title: "Contatti"
index: 5
lastUpdate: "2022-01-29"
---


## Come contattarci?

Se hai domande, se vuoi crearti un conto o se incontri dei problemi, puoi scrivere a %%mailtoEauchatIT%%.  

Se il server ti sembra fuori servizio, le emails verso l'indirizzo eauchat.org potrebberto non funzionare. In questo caso puoi contattarci a %%mailtoEauchatTilaIT%%.  

Se vuoi cifrare il tuo messaggio, puoi usare la chiave pubblica gpg d'eauchat [che puoi scaricare qui](%%gpgKeyDownload%%).  
