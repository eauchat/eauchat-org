---
title: "Contribuire"
index: 4
lastUpdate: "2022-01-29"
---


## Quindi cos'é questo eauchat?

Ecco, eauchat é un server, cioé un computer che resta sempre acceso e sempre connesso ad internet. Nel server sono installati vari servizi (applicazioni e altri), che tu puoi usare come vuoi e da dove vuoi. Questo server é a Toulouse, in uno dei locali di Tetaneutral, in una stanza dove si trovano anche altri molti altri server. Tetaneutral é un fornitore di servizi internet, associativo e indipendente.

Quindi si, questo server comporta certi costi. Oltre ai 720€ e più, iniziali per comprare i pezzi e costruire la macchina, paghiamo 260€/anno a tetaneutral per ricoprire i loro costi in elettricità, larghezza di banda, e questo genere di cose. A questo si aggiunge anche il costo del nome di dominio \"eauchat.org\" che costa una ventina di euro per anno (sisi, un nome di dominio si affitta ;)).

Ma eauchat prende soprattutto del tempo, tutto il tempo che si passa a costruire il server, i servizi, a tenerli aggiornati, a risolvere i problemi che spuntano, rispondere alle domande di chi lo utilizza...é difficile stimare il tempo che spendiamo su eauchat, siccome le cose da fare sono quasi sempre diverse. In generale sono circa dieci ore per settimana, o molte di più se si vogliono aggiungere servizi o se i problemi da risolvere sono grandi grandi.


## Com'é possibile aiutarvi?

Tutte le contribuzioni sono super benvenute 🙂

Se vuoi contribuire alle spese, puoi farlo direttamente quando ci incontri o inviandoci un assegno. Altrimenti attraverso un transfer o un pagamento online (le indicazioni necessarie sono in basso di questa pagina). Puoi dare un contributo in una volta sola o uno più ricorrente.

E se vuoi partecipare in altri modi, inviaci una piccola email al %%mailtoEauchatIT%%, e sono sicuro che si troverà qualcosa di carino che tu puoi fare con le tue capacità e motivazioni!

Ad ogni modo, già utilizzando eauchat o ogni altro server alternativo e indipendete, invece che quello che ci é proposto dalle copagnie che cercano di modellare il mondo connesso di domani, stai già contribuento enormemente nel creare un mondo un po' migliore 🙂


## Per fare un dono

#### Per transfer:

Per il momento, eauchat non ha un conto bancario, quindi se vuoi fare un transfer, inviaci un'email al %%mailtoEauchatIT%% e possiamo inviarti il nostro conto privato.

#### Inviare un bonifico:

Per il momento, come per i pagamenti, inviaci un'email al %%mailtoEauchatIT%% e vi invieremo un nostro conto sul quale effettuare il pagamento.
