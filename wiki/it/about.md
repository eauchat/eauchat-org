---
title: "A proposito"
index: 1
lastUpdate: "2022-01-29"
---


## Perché eauchat?

Eauchat é un server associativo, costruito da volontari per promuovere un internet decentralizzato e libero.  
Se vuoi utilizzare i nostri servizi (mail, cloud, siti, condivisione di documenti, chat e chiamate...) puoi scriverci al %%mailtoEauchatIT%% inviandoci una semplice richiesta. Tieni presente comunque che non siamo google, che non disponiamo quindi di un esercito di ingenieri che lavorano per noi giorno e notte per assicurare il servizio. Gestiamo eauchat al meglio che riusciamo con le capacità che abbiamo.


## Cosa posso fare con eauchat?

 - inviare e ricevere delle emails
 - chattare con chi vuoi (non esclusivamente utilizzatori di eauchat)
 - salvare i tuoi documenti, condividerli e lavorarci su collettivamente
 - sincronizzare e salvare i tuoi contatti e il tuo calendario
 - lavorare collaborativamente su documenti
 - creare e gestire delle liste di email
 - fare sondaggi per organizzarsi collettivamente
 - creare il tuo proprio sito internet (con wordpress o da zero)

Per creare una mailing list o un sito internet, chiedete a %%mailtoEauchatIT%%.  
Se vuoi usare altri servizi, puoi scriverci per proporcene e si vedrà cosa si puo fare per integrarli :)  
