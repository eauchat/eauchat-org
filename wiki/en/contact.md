---
title: "Contact"
index: 5
lastUpdate: "2022-01-29"
---


## How to contact us?

To request an account, support, if you encounter some problems, or for any other request, the easiest way to contact us is to write to %%mailtoEauchatEN%%.  

If for some reason, the server is out of service, emails to eauchat.org addresses may not function, so you can write to %%mailtoEauchatTilaEN%%.  

If you want to send us an ecrypted email, you will need eauchat's gpg public key to encrypt your message. [You can download it here.](%%gpgKeyDownload%%)  
