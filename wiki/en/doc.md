---
title: "Configuration/Documentation"
index: 2
lastUpdate: "2022-01-29"
---


## Configuration / Documentation

- [How to change your password](?p=password)
- [How to configure a mail client](?p=mail)
- [How to use the chat (setup a matrix client)](?p=matrix)
