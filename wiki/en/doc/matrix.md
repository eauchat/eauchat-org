---
title: "how to use the chat"
index: 2.3
sub: "doc"
lastUpdate: "2022-01-29"
---


## How to use the eauchat chat (how to setup a matrix client)

To chat through eauchat, you can visit [this page](%%page_elementBrowser%%), or use a matrix client and configure it following the instructions bellow.

You can use for example Element which provides an [app for smartphones](%%page_elementAndroid%%) or [for computer](%%page_elementDesktop%%).

To log in, just use your usual username and password, but don’t forget to choose "eauchat.org" as "Homeserver".

For more details, see below.

### On the computer

%%image_matrixConfig%%

(1) Click to edit the "Homeserver" option  
(2) The server address is: "eauchat.org"  
(3) Enter your usual eauchat username and password  
(4) Click the "Sign in" button  

### On the phone

%%image_matrixConfigAndroid%%

(1) Choose the "Other" option  
(2) The server address is: "eauchat.org"  
(3) Click on "Sign in"  
(4) Enter your usual eauchat username and password  
(5) Click the "Sign in" button  
