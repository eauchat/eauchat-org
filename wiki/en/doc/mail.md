---
title: "configure a mail client"
index: 2.2
sub: "doc"
lastUpdate: "2022-01-29"
---


## How to configure a mail client

If you want to configure a mail client (K9-mail, Thunderbird…) you may need to choose manual configuration and fill in the configuration as bellow:

| Protocol | Server      | Port | Encryption | Authentication  | Login                           |  
| -------- | ----------- | ---- | ---------- | --------------- | ------------------------------- |  
| IMAP     | eauchat.org | 993  | SSL/TLS    | Normal password | username (without @eauchat.org) |  
| SMTP     | eauchat.org | 587  | STARTTLS   | Normal password | username (without @eauchat.org) |  

If you don’t really understand how to do this setup, you can find a step by step tutorial in [the following page](https://yunohost.org/en/email_configure_client).
