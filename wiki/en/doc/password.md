---
title: "change password"
index: 2.1
sub: "doc"
lastUpdate: "2022-01-29"
---


## How to change your password

You can change your password from the main page, following the instructions below:

%%image_changePassword%%
