---
title: "Contribute"
index: 4
lastUpdate: "2022-01-29"
---


## So what is this eauchat thing?

Well it’s a server, hmm, well, and a server is just a computer that’s always on and connected to internet, in which there are services installed (apps and other things), and you can use them as you like from wherever you want. This server is in Toulouse, in one of Tetaneutral's offices, in a room where lots of other servers are hosted. Tetaneutral, is an associative independent internet provider.

So yes, to run this server has some costs. In addition to the initial buying of parts to make the machine which costed in total 720€, we pay 260€/year to tetaneutral for their expenses and work (electricity, bandwidth, those kinds of things). To those costs, add 20€/year to own the domain eauchat.org (yes if you want a domain name, you must rent it ;)).

But eauchat it’s also time, a lot of time invested, to build up the server, the services, to keep them updated, solve issues that come up, answer questions of users… It’s hard to estimate how much time we spend on it, because there are a lot of different tasks. But it’s always at least a few hours a week. Easily around ten, or much more when deploying new services, or if important issues come up.


## And it’s possible to help you?

Well yes all contributions are very welcome 🙂

If you want to contribute to the expenses, you can give it directly when you meet one of us, send us some check, make a money transfer or an online payment (see below for more info). You can make a one time donation, or a recurrent one.

And if you want to get involved in other ways, just send us a little email to %%mailtoEauchatEN%%, and I’m sure we’ll find some nice things you can do with your abilities and motivation!

Ok, but in any case, don’t forget that just to be using the services of eauchat or any other selfhosting alternative instead of what offer one of the huge companies that are modelling tomorrows digital world, is already an awesome contribution to creating a world a little bit better 🙂


## To donate

#### By bank transfer:

For now, eauchat doesn't have a bank account, so if you want to make a transfer, send us an email to %%mailtoEauchatEN%% and we can send the private bank contact of one of us.

#### Sending us a cheque:

For now, as for bank transfers, send us a little mail to %%mailtoEauchatEN%% and we'll send you an address where you can send it to.
