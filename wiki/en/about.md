---
title: "About"
index: 1
lastUpdate: "2022-01-29"
---


## Why eauchat?

Eauchat is an associative server built by volunteers in order to promote an open, free and decentralized internet.  
If you want to use our services (mail, cloud, websites, file sharing, messenger and calls…) you can write a simple request at %%mailtoEauchatEN%%. However keep in mind that we are not google, we have no army of engineers working night and day. We run this service the best we can and with our (limited) abilities.  


## What can I use eauchat for?

 - sending and receiving emails
 - chat with people (they don't have to necessarily use eauchat)
 - store documents, share them, and work collectively on them
 - synchronize and backup my contacts and calendar
 - create collaborative pads
 - create mailing lists
 - make polls to organize with other people
 - create my own website (with wordpress or from scratch)

For creating a new mailing list or website, you'll have to write a request to %%mailtoEauchatEN%%.  
If there are other services you'd appreciate, feel free to write to us and we'll see if we can manage doing that :)  
