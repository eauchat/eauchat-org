
# eauchat.org homepage and documentation pages

If you make any change, you need to rebuild the `dist` files for changes to take effect.
Do this with:
`./scripts/buildDists.sh` or `bash ./scripts/buildDists.sh`

To test your changes you can use `./scripts/serve.sh`, that will just start a basic server on port 1337, then you can just visit [http://localhost:1337/](http://localhost:1337/) in your browser.
